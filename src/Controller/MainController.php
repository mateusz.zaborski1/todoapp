<?php

namespace App\Controller;

use App\Entity\Container;
use App\Entity\Todo;
use App\Form\ContainerType;
use App\Form\DeleteContainerType;
use App\Form\TodoType;
use App\Repository\ContainerRepository;
use App\Repository\TodoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{
    private $containerRepository;
    private $todoRepository;
    private $em;

    public function __construct(ContainerRepository $containerRepository, TodoRepository $todoRepository, EntityManagerInterface $em){
        $this->containerRepository = $containerRepository;
        $this->todoRepository = $todoRepository;
        $this->em = $em;
    }

    #[Route('/home', name: 'app_main', methods: ['GET', 'POST'])]
    public function index(Request $request): Response
    {
        $user = $this->getUser();
        $containers = $user->getContainers()->toArray();
        
        $container = new Container;

        $form = $this->createForm(ContainerType::class, $container);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $container = $form->getData();
            $container->setUser($user);
            
            $this->em->persist($container);
            $this->em->flush();

            return $this->redirectToRoute('app_main');
        }
        
        return $this->render('main/index.html.twig', [
            'containers' => array_reverse($containers),
            'createContainer' => $form->createView(),
        ]);
    }

    #[Route('/home/list/{id}', name: 'app_show', methods: ['GET', 'POST'])]
    public function show($id, Request $request): Response
    {
        $user = $this->getUser();

        $container = $this->containerRepository->find($id);

        if (!$container) {
            return $this->redirectToRoute('app_main');
        }

        if ($user != $container->getUser()) {
            return $this->redirectToRoute('app_main');
        }

        $todos = $container->getTodos()->toArray();

        $todo = new Todo;

        $form = $this->createForm(TodoType::class, $todo);
        
        $deleteForm = $this->createForm(DeleteContainerType::class, null, [
            'action' => $this->generateUrl('app_destroy', ['id' => $container->getId()]),
        ]);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $todo = $form->getData();
            $todo->setDone(false);
            $todo->setContainer($container);
            
            $this->em->persist($todo);
            $this->em->flush();

            return $this->redirect($this->generateUrl('app_show', ['id' => $id]));
        }

        return $this->render('main/show.html.twig', [
            'container' => $container,
            'todos' => $todos,
            'createTodo' => $form->createView(),
            'deleteForm' => $deleteForm->createView(),
        ]);
    }

    #[Route('/home/list/update/{id}', name: 'app_update', methods: ['PATCH'])]
    public function update($id, Request $request): Response
    {
        $todo = $this->todoRepository->find($id);
        
        if ($todo) {

            if ($this->getUser() == $todo->getContainer()->getUser()) {

                $todo->setDone($request->get('done') ? true : false);
    
                $this->em->flush();

            }

        }

        return $this->redirect($this->generateUrl('app_show', ['id' => $todo->getContainer()->getId()]));
    }

    #[Route('/home/list/delete/{id}', name: 'app_destroy', methods: ['DELETE'])]
    public function destroy($id): Response
    {
        $container = $this->containerRepository->find($id);

        if ($container) {
            
            if ($this->getUser() == $container->getUser()) {

                $this->em->remove($container);
                $this->em->flush();

            }

        }

        return $this->redirectToRoute('app_main');
    }
}
